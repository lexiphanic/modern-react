export default (props) => (
    <div className="notification-block notification-block-style-2">
        <div className="notification-block-dwrapper">
          <div className="notification-block-container">
            <div className="notification-block-content light-color text-center">
              { props.children }
            </div>
          </div>
        </div>
      </div>
  );
