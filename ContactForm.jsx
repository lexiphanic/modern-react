import Link from 'next/link';
import { Component } from 'react'
import 'isomorphic-fetch';

class Index extends Component {
  constructor (props) {
    super(props)

    this.state = {
        name: '',
        email: '',
        subject: '',
        message: '',
        submitting: false,
        submitted: false,
        errored: false,
        site: props.site
    };
    this.submitForm = this.submitForm.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
  }

  submitForm (e) {
    e.preventDefault();
    if (this.state.submitting) {
        return;
    }
    this.setState({
        submitting: true,
        submitted: false,
        errored: false,
    });

    fetch('https://europe-west1-lexiphanic-202918.cloudfunctions.net/mail?ajax=true', {
      method: 'post',
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        subject: this.state.subject,
        message: this.state.message,
        _site: this.state.site,
      }),
    }).then((res) => {
      this.setState({
          submitting: false,
          submitted: res.status === 200,
          errored: res.status !== 200,
      });
    }).catch((e) => {
      this.setState({
          submitting: false,
          submitted: false,
          errored: true,
      });
    })
  }

  handleFieldChange(event) {
    let {name: fieldName, value} = event.target;

    this.setState({
      [fieldName]: value
    });
  };

  render () {
      return (<form method="post" onSubmit={this.submitForm}>
      {this.state.submitted && <div className="gfort-form-alert-message col-md-12">
            <div className="alert alert-gfort alert-success">
                <div className="form-respones-message">
                    <div className="success-message">Your message has been sent!</div>
                </div>
            </div>
        </div>}
        {this.state.errored && <div className="gfort-form-alert-message col-md-12">
              <div className="alert alert-gfort alert-warning">
                  <div className="form-respones-message">
                      <div className="success-message">Your message failed to be sent!</div>
                  </div>
              </div>
          </div>}
  <input type="hidden" name="_site" defaultValue={this.state.site} />
  <div className="col-md-4">
    <div className="form-group">
      <label htmlFor="name">Name *</label>
      <input type="text" className="form-control" name="name" id="name" onChange={this.handleFieldChange} />
    </div>
  </div>
  <div className="col-md-4">
    <div className="form-group">
      <label htmlFor="email">Email Address *</label>
      <input type="email" className="form-control" name="email" id="email" onChange={this.handleFieldChange} />
    </div>
  </div>
  <div className="col-md-4">
    <div className="form-group">
      <label htmlFor="subject">Subject *</label>
      <input type="text" className="form-control" name="subject" id="subject" onChange={this.handleFieldChange} />
    </div>
  </div>
  <div className="col-md-12">
    <div className="form-group">
      <label htmlFor="message">Message *</label>
      <textarea className="form-control" name="message" id="message" onChange={this.handleFieldChange} />
    </div>
  </div>
  <div className="col-md-12">
    <div className="form-group">
      <div className="gfort-recaptcha" data-sitekey="6LdHCQwTAAAAAK0HvYvQJ5oA_9W-vlv5A41xBEGp" />
    </div>
  </div>
  <div className="col-md-12">
    <div className="form-group">
      <button disabled={this.state.sending} type="submit" className="btn btn-gfort">{this.state.sending ? 'Sending Message' : 'Send Message'}</button>
    </div>
  </div>
</form>

)
}
}

export default Index;
