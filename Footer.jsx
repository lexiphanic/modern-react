import Link from 'next/link';

export default (props) => (
  <footer className="footer-section">
    <div className="footer-top-section">
      <div className="footer-top-section-container">
        <div className="container">
          {props.children}
        </div>
      </div>
    </div>
    <div className="footer-copyright-section">
      <div className="footer-copyright-section-container">
        <div className="container">
          <div className="row">
            <div className="copyright-widget widget-left-side">
              <div className="copyright-widget-container">
                <div className="info-block">
                  <div className="info-block-container">
                    <p>© 2019 <Link href="/"><a title={props.name}>{props.name}</a></Link>, all rights reserved.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="copyright-widget widget-right-side">
              <div className="copyright-widget-container">
                <div className="info-block">
                  <div className="info-block-container">
                    <p>Built and Hosted by <a href="https://www.lexiphanic.co.uk" target="_blank">Lexiphanic Limited</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
)
