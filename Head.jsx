import Head from 'next/head';

export default (props) => (
  <div>
    <Head>
        <title>{props.title || null}</title>
        <meta name="author" content="Lexiphanic Limited" />
        <meta name="robots" content="index follow" />
        <meta name="googlebot" content="index follow" />
        <meta httpEquiv="content-type" content="text/html; charset=utf-8" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" href="//t.lexiphanic.co.uk/bundles/lexiphanicthemesplume/images/icons/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CPoppins:400,500,600" />
        <link rel="stylesheet" href="//t.lexiphanic.co.uk/bundles/lexiphanicthemesplume/js/vendor/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="//t.lexiphanic.co.uk/bundles/lexiphanicthemesplume/css/responsive.css" />
        <link rel="stylesheet" href="//t.lexiphanic.co.uk/bundles/lexiphanicthemesplume/css/index/book/style.css" />
    </Head>
  </div>
)
