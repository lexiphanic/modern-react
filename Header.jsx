import { Component } from 'react'

class Index extends Component {
  constructor (props) {
    super(props)

    this.state = {
        children: props.children,
        show: false
    }
  }

  render() {
    return (<header className="header-section fixed-header">
          <div className="header-section-container">
            <div className="header-menu">
              <div className="header-menu-container">
                <nav className="navbar">
                  <div className="container">
                    <div className="row">
                      <div className="col-md-12">
                        {this.state.children[0]}
                        <button onClick={() => {this.setState({show: !this.state.show})}} type="button" className={"navbar-toggle" + (!this.state.show ? " collapsed" : '')} data-toggle="collapse" data-target=".navbar-collapse">
                          <span className="menu-wd">Menu</span>
                          <span className="lines-wrapper"><i className="lines" /></span>
                        </button>

                        <div className={"navbar-collapse" + (!this.state.show ? " collapse" : ' clearfix clear')}>
                            {this.state.children[1]}
                        </div>
                      </div>
                    </div>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </header>
  )}

}

export default Index;
