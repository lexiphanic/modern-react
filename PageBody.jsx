const PageBody = (props) => (
  <div className={`page-body ${props.className}`}>
    <div className="main-content">
      <div className="main-content-container">
        <div className="gfort-section" {...props.gfort}>
          <div className="section-container">
            <div className="container">
              {props.children}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default PageBody
