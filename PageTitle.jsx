const PageTitle = (props) => (
        <div className="page-title-section page-title-section-wide grey-background-color">
          <div className="section-container">
            <div className="breadcrumb-title">
              <div className="container">
                <h1 className="breadcrumb-main-title">{props.title}</h1>
              </div>
            </div>
            {props.children}
          </div>
        </div>
)

export default PageTitle
