export default (props) => (<div id="rev_slider_wrapper" className="rev_slider_wrapper fullwidthbanner-container">
        <div id="rev_slider_container" className="rev_slider fullwidthabanner" style={{position: "relative", width:"100%", paddingTop: "56.25%"}}>
            {props.children}
        </div>
    </div>)
