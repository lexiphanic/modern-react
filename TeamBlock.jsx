export default (props) => (
<div className="team-block">
 <div className="team-block-container">{
   props.members.map((member, i) => {
         return <div key={member.name}><div className={((i % 3) == 0 ? 'col-md-12 ' : '') } /> <div className="col-md-4">
           <div className="team-block">
             <div className="team-block-container">
               <div className="team-block-media">
                 <img src={`/static/team/${member.name} headshot.jpg`} alt={member.name} />
               </div>
               <div className="team-block-body">
                 <div className="team-block-title">
                   <h4 className="team-block-name">{member.name}</h4>
                   <p className="team-block-subtitle">{member.subtitle}</p>
                 </div>
                 <div className="team-block-desc">
                   <p>{member.bio || null}</p>
                 </div>
               </div>
             </div>
           </div>
         </div></div>
     })
}</div></div>
)
